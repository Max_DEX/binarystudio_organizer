﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;



namespace BinaryStudio_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service : IContract
    {
        public string Server_Database()
        {
            return Server_Setting.DataBase;
        }
      /*  public string Server_IP()
        {
            return Server_Setting.IP;
        }*/
        public void Update_Task()
        {
            using (SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase))
            {
                const string sql = ("UPDATE Task SET Compleated = 'True' FROM Task WHERE 'True' = ALL (SELECT Compleated FROM Subtask WHERE Subtask.TaskID=Task.TaskID) AND EXISTS (SELECT * FROM Subtask WHERE Subtask.TaskID=Task.TaskID)");
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);


                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
            }
        }
        public void NonCompleated_Task(int ID)
        {
            using (SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase))
            {
                const string sql = ("UPDATE Task SET Compleated = 'False' FROM Task  WHERE (Task.TaskID =@ID)");
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);
                sqlCmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;


                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }



        }

        public void NonCompleated_SubTask(int ID)
        {

            using (SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase))
            {
                const string sql = ("UPDATE Subtask SET Compleated = 'False' FROM Subtask  WHERE (Subtask.SubtaskID =@ID)");
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);
                sqlCmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;


                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }


        public void Compleated_Subtask(int ID)
        {
            using (SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase))
            {
                const string sql = ("UPDATE Subtask SET Compleated = 'True' FROM Subtask  WHERE (Subtask.SubtaskID =@ID)");
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);
                sqlCmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;


                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
           
        }

        public void Compleated_Task(int ID)
        {
            using (SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase))
            {
                const string sql = ("UPDATE Task SET Compleated = 'True' FROM Task  WHERE (Task.TaskID =@ID)");
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);
                sqlCmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;


                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
           

             
        }
        
        public void Deleted_Task()
        {
            SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase);
            sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;

            string sql = string.Format("DELETE Task FROM Task WHERE ([Task].[Compleated] = 'True')");

            using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
            {
                cmd.ExecuteNonQuery();
            }


            sqlConn.Close();
                }
        public void add_Subtask(int ID, string Text, bool Compleated)
        {
            SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase);
            sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;

            string sql = string.Format("Insert Into dbo.Subtask " +
                  "(TaskID,Text, Compleated) Values('{0}','{1}','{2}')",
                  ID, Text, Compleated);

            using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
            {
                cmd.ExecuteNonQuery();
            }


            sqlConn.Close();
        }



    
    
        public void add_Task(string input1, string input2, string input3, bool input4)
        {
            SqlConnection sqlConn = new SqlConnection(Server_Setting.DataBase);
            sqlConn.Open();

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = sqlConn;

            string sql = string.Format("Insert Into dbo.Task " +
                  "(Category, Text, Date, Compleated) Values('{0}','{1}','{2}','{3}')",
                  input1, input2, input3.ToString() , input4);
            
            using (SqlCommand cmd = new SqlCommand(sql, sqlConn))
            {
                cmd.ExecuteNonQuery();
            }


            sqlConn.Close();
        }


      

    }

   /* public class Task
    {
        long _TaskID;
        string _Text, _Category;
        DateTime _Date;
        bool _Completed;
        public long TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }
        public object[] ToValues()
        {
            object[] a;
            a = new object[5];
            a[0] = TaskID;
            a[1] = Text;
            a[2] = Completed;
            a[3] = Date;
            a[4] = Category;
            return a;
        }*/
    }




