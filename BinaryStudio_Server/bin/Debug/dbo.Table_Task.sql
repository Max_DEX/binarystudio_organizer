﻿CREATE TABLE [dbo].[Task]
(
	[TaskID] INT NOT NULL  IDENTITY, 
    [Category] NVARCHAR(50) NULL, 
    [Text] NVARCHAR(MAX) NULL, 
    [Date] DATE NULL, 
    [Compleat] BIT NULL, 
    CONSTRAINT [PK_Table] PRIMARY KEY ([TaskID])
)
